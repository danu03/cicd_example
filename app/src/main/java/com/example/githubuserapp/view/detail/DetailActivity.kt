package com.example.githubuserapp.view.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.githubuserapp.R
import com.example.githubuserapp.model.UsersGithub
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {
    companion object {
        const val USERS = "users"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val actionBar = supportActionBar
        actionBar!!.title = "Detail User"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)

        val users = intent.getParcelableExtra(USERS) as UsersGithub

        tv_name.text = users.name
        tv_username.text = users.username
        tv_location.text = users.location
        tv_company.text = users.company
        tv_followers.text = users.followers
        tv_following.text = users.following
        tv_repository.text = users.repository
        iv_user.setImageResource(users.photo!!)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}