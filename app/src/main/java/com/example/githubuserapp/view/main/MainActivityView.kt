package com.example.githubuserapp.view.main

import com.example.githubuserapp.model.UsersGithub

interface MainActivityView {
    fun showData(result: ArrayList<UsersGithub>)
    fun data()
    fun addItem()
    fun moveSelectedData(data: UsersGithub)
}