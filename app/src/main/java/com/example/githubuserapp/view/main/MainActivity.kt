package com.example.githubuserapp.view.main

import android.content.Intent
import android.content.res.TypedArray
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubuserapp.R
import com.example.githubuserapp.adapter.UsersGithubAdapter
import com.example.githubuserapp.model.UsersGithub
import com.example.githubuserapp.view.detail.DetailActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    MainActivityView {
    private lateinit var dataName: Array<String>
    private lateinit var dataUsername: Array<String>
    private lateinit var dataRepository: Array<String>
    private lateinit var dataFollowers: Array<String>
    private lateinit var dataFollowing: Array<String>
    private lateinit var dataCompany: Array<String>
    private lateinit var dataLocation: Array<String>
    private lateinit var dataPhoto: TypedArray
    private lateinit var listUsers: ArrayList<UsersGithub>
    private lateinit var adapter: UsersGithubAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listUsers = arrayListOf()
        adapter = UsersGithubAdapter(listUsers)

        data()
        addItem()
        rv_users_github.layoutManager = LinearLayoutManager(this)
        rv_users_github.adapter = adapter
        adapter.setOnClickListener(object : UsersGithubAdapter.OnClickListenerCallback {
            override fun onItemClicked(users: UsersGithub) {
                moveSelectedData(users)
            }
        })
    }

    override fun showData(result: ArrayList<UsersGithub>) {
        listUsers.clear()
        listUsers.addAll(result)
        adapter.notifyDataSetChanged()
    }

    override fun data() {
        dataUsername = resources.getStringArray(R.array.username)
        dataName = resources.getStringArray(R.array.name)
        dataCompany = resources.getStringArray(R.array.company)
        dataLocation = resources.getStringArray(R.array.location)
        dataPhoto = resources.obtainTypedArray(R.array.avatar)
        dataRepository = resources.getStringArray(R.array.repository)
        dataFollowers = resources.getStringArray(R.array.followers)
        dataFollowing = resources.getStringArray(R.array.following)
    }

    override fun addItem() {
        for (position in dataName.indices) {
            val users = UsersGithub(
                dataUsername[position],
                dataName[position],
                dataPhoto.getResourceId(position, -1),
                dataCompany[position],
                dataLocation[position],
                dataRepository[position],
                dataFollowers[position],
                dataFollowing[position]
            )
            listUsers.add(users)
        }
    }

    override fun moveSelectedData(data: UsersGithub) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(DetailActivity.USERS, data)
        startActivity(intent)
    }
}