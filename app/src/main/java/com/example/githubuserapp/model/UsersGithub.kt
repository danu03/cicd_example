package com.example.githubuserapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UsersGithub(
    val username: String?,
    val name: String?,
    val photo: Int?,
    val company: String?,
    val location: String?,
    val repository: String?,
    val followers: String?,
    val following: String?
) : Parcelable