package com.example.githubuserapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.githubuserapp.R
import com.example.githubuserapp.model.UsersGithub
import kotlinx.android.synthetic.main.item_users_github.view.*

class UsersGithubAdapter(private val listUsers: ArrayList<UsersGithub>) :
    RecyclerView.Adapter<UsersGithubAdapter.ViewHolder>() {
    private lateinit var listener: OnClickListenerCallback
    fun setOnClickListener(listenerCallback: OnClickListenerCallback) {
        this.listener = listenerCallback
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): UsersGithubAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_users_github, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = listUsers.size

    override fun onBindViewHolder(holder: UsersGithubAdapter.ViewHolder, position: Int) {
        holder.bind(listUsers[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(usersGithub: UsersGithub) {
            itemView.tv_name.text = usersGithub.name
            itemView.tv_company.text = usersGithub.company
            itemView.tv_location.text = usersGithub.location
            Glide.with(itemView)
                .load(usersGithub.photo)
                .into(itemView.iv_photo)
            itemView.setOnClickListener {
                listener.onItemClicked(usersGithub)
            }
        }
    }

    interface OnClickListenerCallback {
        fun onItemClicked(users: UsersGithub)
    }

}